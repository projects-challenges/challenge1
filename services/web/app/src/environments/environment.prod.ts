export const environment = {
  production: true,
  API: {
    BASE_URL: 'https://app-challenge1-api.herokuapp.com/api/'
  }
};
