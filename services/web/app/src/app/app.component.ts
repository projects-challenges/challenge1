import { Component } from '@angular/core';
import {TranslateService} from "@ngx-translate/core";
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'project';

  constructor(
    private translate: TranslateService,
    private router: Router,
  ) {
    this.translate.setDefaultLang('pt');
    this.translate.use('pt');
    this.router.navigate(['/price']);
  }

  options = {
    min: 8,
    max: 100,
    ease: 'linear',
    speed: 200,
    trickleSpeed: 300,
    meteor: true,
    spinner: true,
    spinnerPosition: 'right',
    direction: 'ltr+',
    color: '#00A8B2',
    thick: true
  };
}
