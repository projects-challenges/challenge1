import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ParkingListComponent } from './parking-list/parking-list.component';
import { ParkingEditDialogComponent } from './parking-edit-dialog/parking-edit-dialog.component';
import {ParkingService} from "./parking.service";
import {CoreModule} from "../../core/core.module";



@NgModule({
  declarations: [ParkingListComponent, ParkingEditDialogComponent],
  providers: [ParkingService],
  imports: [
    CommonModule,
    CoreModule,
  ]
})
export class ParkingModule { }
