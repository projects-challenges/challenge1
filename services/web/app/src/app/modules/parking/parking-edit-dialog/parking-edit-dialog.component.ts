import {Component, Inject, OnInit} from '@angular/core';
import {BaseEditDialogComponent} from "../../../core/base/base.edit.dialog.component";
import {ParkingModel} from "../../../core/models/parking.model";
import {ParkingFilter} from "../../../core/filters/parking.filter";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {ToastrService} from "ngx-toastr";
import {TranslateService} from "@ngx-translate/core";
import {NgxSpinnerService} from "ngx-spinner";
import {ParkingService} from "../parking.service";

@Component({
  selector: 'app-parking-edit-dialog',
  templateUrl: './parking-edit-dialog.component.html',
  styleUrls: ['./parking-edit-dialog.component.scss']
})
export class ParkingEditDialogComponent extends BaseEditDialogComponent<ParkingModel, ParkingFilter> implements OnInit {

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: ParkingModel,
    protected service: ParkingService,
    protected toast: ToastrService,
    protected translate: TranslateService,
    protected spinner: NgxSpinnerService,
    protected dialogRef: MatDialogRef<ParkingEditDialogComponent>,
  ) {
    super()
  }

  ngOnInit(): void {
  }

}
