import { Injectable } from '@angular/core';
import {BaseHttpService} from "../../core/base/base.http.service";
import {ParkingModel} from "../../core/models/parking.model";
import {ParkingFilter} from "../../core/filters/parking.filter";
import {HttpClient, HttpParams} from "@angular/common/http";
import {BasePaginatedResponse} from "../../core/base/base.paginated-response";

@Injectable()
export class ParkingService extends BaseHttpService<ParkingModel, ParkingFilter> {

  constructor(protected httpClient: HttpClient) {
    super();
    this.server_url += 'parking/';
  }

  public getRevenueTotal(filters?: ParkingFilter) {
    let params = new HttpParams();
    if (filters) {
      Object.keys(filters).forEach(function (key) {
        if (filters[key] !== null && filters[key] !== undefined) {
          params = params.append(key, filters[key]);
        }
      });
    }
    return this.httpClient.get(`${this.server_url}revenue_total/`, { params }).toPromise();
  }

}
