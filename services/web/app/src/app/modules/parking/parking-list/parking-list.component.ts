import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {MatSort} from "@angular/material/sort";
import {MatPaginator} from "@angular/material/paginator";
import {MatTableDataSource} from "@angular/material/table";
import {BaseListComponent} from "../../../core/base/base.list.component";
import {ParkingModel} from "../../../core/models/parking.model";
import {ParkingFilter} from "../../../core/filters/parking.filter";
import {ToastrService} from "ngx-toastr";
import {TranslateService} from "@ngx-translate/core";
import {NgxSpinnerService} from "ngx-spinner";
import {MatDialog} from "@angular/material/dialog";
import {ParkingService} from "../parking.service";
import {ParkingEditDialogComponent} from "../parking-edit-dialog/parking-edit-dialog.component";
import {DialogComponent} from "../../../core/components/dialog/dialog.component";
import * as moment from 'moment';

@Component({
  selector: 'app-parking-list',
  templateUrl: './parking-list.component.html',
  styleUrls: ['./parking-list.component.scss'],
})
export class ParkingListComponent extends BaseListComponent<ParkingModel, ParkingFilter> implements OnInit, AfterViewInit  {
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  dataSource: MatTableDataSource<ParkingModel> = new MatTableDataSource();

  constructor(
    protected service: ParkingService,
    protected toast: ToastrService,
    protected translate: TranslateService,
    protected spinner: NgxSpinnerService,
    protected dialog: MatDialog,
  ) {
    super();
    this.filters = new ParkingFilter();
  }
  public revenue_total?: number;
  public date_filter =  {"date_after": "", "date_before": "", "entry_date_filter": true};
  public new() {
    const price = new ParkingModel();
    this.openDialog(price, '600', ParkingEditDialogComponent);
  }

  public search(fromFilters?) {
    if(this.date_filter['date_after']) {
      if(this.date_filter['entry_date_filter']) {
        this.filters.entry_date_after = moment(this.date_filter['date_after']).format('YYYY-MM-DD');
        this.filters.departure_date_after = null;
      } else {
        this.filters.departure_date_after = moment(this.date_filter['date_after']).format('YYYY-MM-DD');
        this.filters.entry_date_after = null;
      }
    } else {
      this.filters.entry_date_after = null;
      this.filters.departure_date_after = null;
    }
    if(this.date_filter['date_before']) {
      if(this.date_filter['entry_date_filter']) {
        this.filters.entry_date_before = moment(this.date_filter['date_before']).format('YYYY-MM-DD');
        this.filters.departure_date_before = null;
      } else {
        this.filters.departure_date_before = moment(this.date_filter['date_before']).format('YYYY-MM-DD');
        this.filters.entry_date_before = null;
      }
    } else {
      this.filters.entry_date_before = null;
      this.filters.departure_date_before = null;
    }
    this.service.getRevenueTotal(this.filters).then(
      response => {
        this.revenue_total = response["revenue_total"];
      });
    this.getList(true);
  }

  public edit(parking: ParkingModel) {
    this.openDialog(parking, '600', ParkingEditDialogComponent);
  }

  public remove(parking: ParkingModel) {
    let data = Object.assign({}, parking);
    let data_new = {
      "id": data.id,
      "itens": [data.vehicle_number, data.model, data.color]
    }
    this.removeById(data_new);
  }

  public async parkingExit(parking: ParkingModel) {
    try {
      parking = await this.service.getById(parking.id)

      let data = Object.assign({}, parking);
      let amount_to_pay = "";
      if(data.price_exit)
        amount_to_pay =  `${this.translate.instant('PARKING.MESSAGE.AMOUNT_TO_PAY')} ${data.price_exit.toFixed(2)}`
      let data_new = {
        "title": this.translate.instant('COMMON.TOAST.TITLE.ATTENTION'),
        "sub_title": this.translate.instant('PARKING.MESSAGE.CONFIRM_EXIT'),
        "id": data.id,
        "itens": [
          data.vehicle_number,
          data.model,
          data.color,
          amount_to_pay
        ]
      }

      const dialogRef = this.dialog.open(DialogComponent, {
        data: data_new
      });
      dialogRef.afterClosed().subscribe(result => {
        if (result === 'data') {
          parking.revenue = parking.price_exit;
          this.spinner.show();
          this.service.updateById(parking, data_new.id).then(
            res => {
              this.spinner.hide();
              this.toast.success(this.translate.instant('COMMON.MESSAGE.SAVE.SUCCESS'));
              this.filters = {} as ParkingFilter;
              this.filters.page = 1;
              this.search(true);
            }
          );
        }
      });
    } catch (error) {
      this.spinner.hide();
      this.toast.error(this.translate.instant('COMMON.MESSAGE.GENERIC_ERROR'));
      console.error(error);
    } finally {
      this.spinner.hide();
    }

  }

  ngOnInit() {
    super.ngOnInit();
  }

  ngAfterViewInit() {
    this.service.getRevenueTotal(this.filters).then(
      response => {
        this.revenue_total = response["revenue_total"];
      });
    super.ngAfterViewInit();
  }

}
