import { Injectable } from '@angular/core';
import {BaseHttpService} from "../../core/base/base.http.service";
import {PriceModel} from "../../core/models/price.model";
import {PriceFilter} from "../../core/filters/price.filter";
import {HttpClient} from "@angular/common/http";

@Injectable()
export class PriceService extends BaseHttpService<PriceModel, PriceFilter> {

  constructor(protected httpClient: HttpClient) {
    super();
    this.server_url += 'price/';
  }
}
