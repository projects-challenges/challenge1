import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PriceListComponent } from './price-list/price-list.component';
import { PriceEditDialogComponent } from './price-edit-dialog/price-edit-dialog.component';
import {CoreModule} from "../../core/core.module";
import {PriceService} from "./price.service";

@NgModule({
  declarations: [PriceListComponent, PriceEditDialogComponent],
  providers: [PriceService],
  imports: [
    CommonModule,
    CoreModule,
  ]
})
export class PriceModule { }
