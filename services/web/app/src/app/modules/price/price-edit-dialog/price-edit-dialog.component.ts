import {Component, Inject, OnInit} from '@angular/core';
import {PriceModel} from "../../../core/models/price.model";
import {PriceFilter} from "../../../core/filters/price.filter";
import {PriceService} from "../price.service";
import {ToastrService} from "ngx-toastr";
import {TranslateService} from "@ngx-translate/core";
import {NgxSpinnerService} from "ngx-spinner";
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from "@angular/material/dialog";
import {BaseEditDialogComponent} from "../../../core/base/base.edit.dialog.component";
import {WeekDay} from "@angular/common";
import * as moment from 'moment';

@Component({
  selector: 'app-price-edit-dialog',
  templateUrl: './price-edit-dialog.component.html',
  styleUrls: ['./price-edit-dialog.component.scss'],
})
export class PriceEditDialogComponent extends BaseEditDialogComponent<PriceModel, PriceFilter> implements OnInit {

  public weekDaySunday = 7;
  public weekDays: WeekDay[] = [WeekDay.Monday, WeekDay.Tuesday, WeekDay.Wednesday, WeekDay.Thursday, WeekDay.Friday, WeekDay.Saturday, this.weekDaySunday];

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: PriceModel,
    protected service: PriceService,
    protected toast: ToastrService,
    protected translate: TranslateService,
    protected spinner: NgxSpinnerService,
    protected dialogRef: MatDialogRef<PriceEditDialogComponent>,
  ) {
    super()
  }

  ngOnInit(): void {
  }

}
