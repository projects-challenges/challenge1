import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import {NgxSpinnerService} from "ngx-spinner";
import {TranslateService} from "@ngx-translate/core";
import {PriceService} from "../price.service";
import {BaseListComponent} from "../../../core/base/base.list.component";
import {PriceModel} from "../../../core/models/price.model";
import {PriceFilter} from "../../../core/filters/price.filter";
import {MatSort} from "@angular/material/sort";
import {MatPaginator} from "@angular/material/paginator";
import {MatTableDataSource} from "@angular/material/table";
import {MatDialog} from "@angular/material/dialog";
import {PriceEditDialogComponent} from "../price-edit-dialog/price-edit-dialog.component";

@Component({
  selector: 'app-price-list',
  templateUrl: './price-list.component.html',
  styleUrls: ['./price-list.component.scss'],
})
export class PriceListComponent extends BaseListComponent<PriceModel, PriceFilter> implements OnInit, AfterViewInit {

  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  dataSource: MatTableDataSource<PriceModel> = new MatTableDataSource();

  constructor(
    protected service: PriceService,
    protected toast: ToastrService,
    protected translate: TranslateService,
    protected spinner: NgxSpinnerService,
    protected dialog: MatDialog,
  ) {
    super();
  }

  public new() {
    const price = new PriceModel();
    this.openDialog(price, '600', PriceEditDialogComponent);
  }

  public edit(price: PriceModel) {
    this.openDialog(price, '600', PriceEditDialogComponent);
  }

  public remove(price: PriceModel) {
    let data = Object.assign({}, price);
    let data_new = {
      "id": data.id,
      "itens": [
        this.translate.instant('COMMON.CALENDAR.WEEKDAYS.'+ data.week_day_start +''),
        this.translate.instant('COMMON.CALENDAR.WEEKDAYS.'+ data.week_day_end +''),
        data.hour_start,
        data.hour_end,
        'R$ ' +data.price_per_hour.toFixed(2)
      ]
    }
    this.removeById(data_new);
  }

  ngOnInit() {
    super.ngOnInit();
  }

  ngAfterViewInit() {
    super.ngAfterViewInit();
  }
}
