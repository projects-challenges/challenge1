import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {NavComponent} from "./core/components/nav/nav.component";
import {PriceListComponent} from "./modules/price/price-list/price-list.component";
import {ParkingListComponent} from "./modules/parking/parking-list/parking-list.component";

const routes: Routes = [
  {
    path: '',
    component: NavComponent,
    children: [
      {
        path: 'price', component: PriceListComponent
      },
      {
        path: 'parking', component: ParkingListComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
