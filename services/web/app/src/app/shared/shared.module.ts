import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import {MaterialModule} from "./material.module";


@NgModule({
  imports: [
    MaterialModule,
    CommonModule,
    ReactiveFormsModule
  ],
  declarations: [
  ],
  exports: [
  ]
})
export class SharedModule { }
