import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { TranslateService } from '@ngx-translate/core';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import {ToastrService} from "ngx-toastr";

@Injectable()
export class HttpInterceptorService implements HttpInterceptor {

  constructor(
    public toast: ToastrService,
    public translate: TranslateService,
    public router: Router,
    public spinner: NgxSpinnerService) {
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    const accept_language = 'pt';
    const headers = { 'Accept-Language': accept_language };
    const request = req.clone({ setHeaders: headers });

    return next.handle(request).pipe(tap((event: HttpEvent<any>) => {
      if (event instanceof HttpResponse) {
      }
    }, (errorResponse: HttpErrorResponse) => {
      console.log(errorResponse);
      this.handleError(errorResponse);
    }));
  }

  private handleError(httpErrorResponse: HttpErrorResponse) {
    this.spinner.hide();

    if (httpErrorResponse.status === 0) {
      this.toast.error(this.translate.instant('COMUNS.MESSAGE.ERROR_MESSAGE_CONNECTION'));
    } else if (httpErrorResponse['error']['title'] != null) {
      this.toast.error(this.translate.instant(httpErrorResponse['error']['detail']));
      return;
    } else if (httpErrorResponse['error']['detail'] != null) {
      this.toast.error(this.translate.instant(httpErrorResponse['error']['detail']));
      return;
    } else {
      this.toast.error(httpErrorResponse.message.toString());
      return;
    }
  }

}
