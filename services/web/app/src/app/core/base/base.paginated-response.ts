export class BasePaginatedResponse<T> {
  public count: number;
  public next: number;
  public previous: number;
  public results: Array<T>;
}
