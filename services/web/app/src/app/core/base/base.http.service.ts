import { BasePaginatedResponse } from './base.paginated-response';
import { BaseFilter } from './base.filter';
import { environment } from '../../../environments/environment';
import { HttpClient, HttpParams } from '@angular/common/http';
import { BaseResponse } from './base.response';
import {BaseModel} from "./base.model";

export class BaseHttpService<T extends BaseModel, Z extends BaseFilter> {

    protected httpClient: HttpClient;
    protected server_url;

    constructor() {
        this.server_url = environment.API.BASE_URL;
    }

    public getServerUrl(): string {
        return this.server_url;
    }

    public setServerUrl(server_url: string) {
        this.server_url = server_url;
    }

    public getList(filters?: Z) {
        let params = new HttpParams();
       if (filters) {
            Object.keys(filters).forEach(function (key) {
                if (filters[key] !== null && filters[key] !== undefined) {
                    params = params.append(key, filters[key]);
                }
            });
        }
        return this.httpClient.get<BasePaginatedResponse<T>>(this.server_url, { params }).toPromise();
    }

    public getListWithUrl(url: string, filters?: Z) {
        let params = new HttpParams();
        if (filters) {
            Object.keys(filters).forEach(function (key) {
                params = params.append(key, filters[key]);
            });
        }
        return this.httpClient.get<BasePaginatedResponse<T>>(url, { params }).toPromise();
    }

    public getAll(filters?: Z, url?: string) {
        if (!filters) {
            filters = (<Z>{});
        }
        let params = new HttpParams();
        Object.keys(filters).forEach(function (key) {
            if (filters[key] !== null && filters[key] !== undefined) {
                params = params.append(key, filters[key]);
            }
        });
        if (!url) {
            url = this.server_url;
        }
        return this.httpClient.get<BasePaginatedResponse<T>>(url, { params: params }).toPromise();
    }

    public getDetail(id: number) {
        return this.httpClient.get<BaseResponse<T>>(`${this.server_url}${id}/`).toPromise();
    }

    public getById(id: number, url?, filters?) {
        if (!url) {
            url = this.server_url;
        }
        let params = new HttpParams();
        if (filters) {
            Object.keys(filters).forEach(function (key) {
                if (filters[key] !== null && filters[key] !== undefined) {
                    params = params.append(key, filters[key]);
                }
            });
        }
        return this.httpClient.get<T>(`${url}${id}/`, { params: params }).toPromise();
    }

    public getByUrl(url: string) {
        return this.httpClient.get<BasePaginatedResponse<T>>(url).toPromise();
    }

    public create(object: T) {
        delete object.id;
        delete object.url;
        return this.httpClient.post<T>(this.server_url, object).toPromise();
    }

    public createOrUpdate(object: T) {
        return this.httpClient.patch<T>(this.server_url, object).toPromise();
    }

    public update(object: T) {
        return this.httpClient.patch<T>(object.url, object).toPromise();
    }

    public updateById(object: T, id: number) {
        const url = `${this.server_url}${id}/`;
        return this.httpClient.patch<T>(url, object).toPromise();
    }

    public delete(object: T) {
        return this.httpClient.delete<BasePaginatedResponse<T>>(object.url).toPromise();
    }

    public deleteById(id: number) {
        const url = `${this.server_url}${id}/`;
        return this.httpClient.delete(url).toPromise();
    }


    public searchEntries(term, key, url?) {
        let params = new HttpParams();
        params = params.append(key, term);
        if (!url) {
            url = this.server_url;
        }
        return this.httpClient
            .get<BasePaginatedResponse<T>>(url, { params });
    }
}
