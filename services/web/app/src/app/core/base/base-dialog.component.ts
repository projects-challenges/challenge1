// import {Inject, OnDestroy, OnInit} from '@angular/core';
// import {TranslateService} from '@ngx-translate/core';
// import {ToastrService} from "ngx-toastr";
// import {BaseModel} from "./base.model";
// import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
// import {BaseHttpService} from "./base.http.service";
// import {BaseFilter} from "./base.filter";
// import {BaseComponent} from "./base.component";
//
//
// export abstract class BaseDialogComponent<T extends BaseModel, Z extends BaseFilter> extends BaseComponent implements OnInit, OnDestroy {
//
//   public object: any;
//   public saveLoading: boolean;
//   public loading: boolean;
//   public noResults: boolean;
//
//   protected service: BaseHttpService<T, Z>;
//   protected toastr: ToastrService;
//
//   constructor() {
//     super();
//   }
//
//   ngOnInit() {
//     if (!this.object) {
//       this.object = <T>{};
//     }
//     this.localInit();
//   }
//
//   protected localInit() { }
//
//   public ngOnDestroy() { }
//   // public save(): void {
//   //   if (!this.data.id) {
//   //     this.service.save(this.data).subscribe(
//   //       (response) => {
//   //         this.showSucessMessage();
//   //       });
//   //   } else {
//   //     this.service.update(this.data.id, this.data).subscribe(
//   //       (response) => {
//   //         this.showSucessMessage();
//   //       });
//   //   }
//   // }
//   //
//   // public containsField(body, filds: string[]) {
//   //   let exist = false;
//   //   for (const field of filds) {
//   //     if (body.hasOwnProperty(field)) {
//   //       exist = true;
//   //       break;
//   //     }
//   //   }
//   //   return exist;
//   // }
//   //
//   // public msgField(body, filds: string[]): string[] {
//   //   const msgs: string[] = [];
//   //   for (const field of filds) {
//   //     if (body.hasOwnProperty(field)) {
//   //       const fildsMsgs = body[field];
//   //       for (const fieldmsg of fildsMsgs) {
//   //         msgs.push(fieldmsg);
//   //       }
//   //     }
//   //   }
//   //   return msgs;
//   // }
//   //
//   // public showSucessMessage(): void {
//   //   this.dialogRef.close(true);
//   //   this.toast.success(this.translate.instant('COMUNS.TOAST.TITLE.SUCESS'), this.translate.instant('COMUNS.MESSAGE.SAVE.SUCESS'));
//   // }
//   //
//   // public closeDialog(): void {
//   //   this.dialogRef.close(false);
//   // }
//
// }
