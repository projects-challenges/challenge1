import {Inject, Injectable, OnInit} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {BaseModel} from "./base.model";
import {BaseHttpService} from "./base.http.service";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {ToastrService} from "ngx-toastr";
import {NgxSpinnerService} from "ngx-spinner";
import {BaseFilter} from "./base.filter";

@Injectable()
export class BaseEditDialogComponent<T extends BaseModel, Z extends BaseFilter>implements OnInit {

  protected service: BaseHttpService<T, Z>;
  protected dialogRef: MatDialogRef<any>
  protected toast: ToastrService;
  protected translate: TranslateService;
  protected spinner: NgxSpinnerService;

  @Inject(MAT_DIALOG_DATA) public data: T;
  public filters: Z;

  constructor() {}

  ngOnInit() {
  }

  public async save(object?) {
    try {
      this.spinner.show();
      let response;
      let data = Object.assign(<T>{}, this.data);

      if(object) {
        data = Object.assign({}, object);
      }

      if (data.id) {
        response = await this.service.updateById(data, data.id);
        this.showSucessMessage();
      } else {
        response = await this.service.create(data);
        this.showSucessMessage();
      }
    } catch (error) {
      this.spinner.hide();
      console.error(error);
    } finally {
      this.spinner.hide();
    }
  }

  public showSucessMessage(): void {
    this.dialogRef.close(true);
    this.toast.success(this.translate.instant('COMMON.MESSAGE.SAVE.SUCCESS'));
  }

  public closeDialog(): void {
    this.dialogRef.close(false);
  }
}
