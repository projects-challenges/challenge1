export class BaseModel {
  id?: number;
  url?: string;
  created_at?: string;
  modified_at?: string;
}
