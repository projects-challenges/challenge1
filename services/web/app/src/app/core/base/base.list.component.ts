import {BaseModel} from "./base.model";
import {BaseFilter} from "./base.filter";
import {AfterViewInit, Injectable, OnInit, ViewChild} from "@angular/core";
import {BaseHttpService} from "./base.http.service";
import {ToastrService} from "ngx-toastr";
import {MatPaginator, PageEvent} from "@angular/material/paginator";
import {MatSort} from "@angular/material/sort";
import {MatTableDataSource} from "@angular/material/table";
import {MatDialog} from "@angular/material/dialog";
import {TranslateService} from "@ngx-translate/core";
import {NgxSpinnerService} from "ngx-spinner";
import {DialogComponent} from "../components/dialog/dialog.component";

@Injectable()
export class BaseListComponent<T extends BaseModel, Z extends BaseFilter> implements OnInit, AfterViewInit {

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort)

  protected service: BaseHttpService<T, Z>;
  protected dialog: MatDialog;
  protected toast: ToastrService;
  protected translate: TranslateService;
  protected spinner: NgxSpinnerService;

  public objects: Object[];
  public filters: Z;
  public pageEvent = PageEvent;
  public dataSource: MatTableDataSource<T> = new MatTableDataSource();
  public resultList: T[];

  constructor() {
  }

  public openDialog(object: T, width: string, dialogComponent: any): void {
    const data = Object.assign({}, object);
    width = width + 'px';
    this.dialog.open(dialogComponent, {
      width: width,
      data: data,
      disableClose: true

    }).afterClosed().subscribe(result => {
      if (result) {
        this.getList(true);
      }
    });
  }

  public async getList(fromFilters?) {
    try {
      this.spinner.show();
      if (fromFilters) {
        this.filters.page = 1;
      }
      this.filters.limit = this.paginator.pageSize;
      this.filters.page = this.paginator.pageIndex;
      this.filters.offset = this.paginator.pageSize * this.paginator.pageIndex;
      this.objects = new Array<T>();
      const response = await this.service.getList(this.filters);
      if (response.results) {
        this.resultList = response.results;
        this.paginator.length = response.count;
        this.dataSource = new MatTableDataSource(this.resultList);
      } else {
        this.spinner.hide();
      }
    } catch (error) {
      this.spinner.hide();
      console.error(error);
    } finally {
      this.spinner.hide();
    }
  }

  public removeById(object) {
    try {
      let data = Object.assign({}, object);
      data.title = this.translate.instant('COMMON.TOAST.TITLE.ATTENTION');
      data.sub_title = this.translate.instant('COMMON.MESSAGE.DELETE.CONFIRM');
      const dialogRef = this.dialog.open(DialogComponent, {
        data: data
      });
      dialogRef.afterClosed().subscribe(result => {
        if (result === 'data') {
          this.spinner.show();
          this.service.deleteById(object.id).then(
            res => {
              this.spinner.hide();
              this.toast.success(this.translate.instant('COMMON.MESSAGE.DELETE.SUCCESS'));
              this.filters = {} as Z;
              this.filters.page = 1;
              this.getList(true);
            }
          );
        }
      });
    } catch (error) {
      this.spinner.hide();
      this.toast.error(this.translate.instant('COMMON.MESSAGE.DELETE.ERROR'));
      console.error(error);
    } finally {
      this.spinner.hide();
    }
  }

  getPageSizeOptions(): number[] {
    return  [10, 20, 30, 40, 50];
  }

  getPageSize(): number {
    return  10;
  }

  getPageIndex(): number {
    return 0;
  }

  ngAfterViewInit() {
    this.getList(true);
  }

  ngOnInit() {
    this.objects = new Array<T>();
    this.filters = {} as Z;
  }

}
