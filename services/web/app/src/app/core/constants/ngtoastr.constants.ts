export const ngxToastr = {
    timeOut: 3000,
    maxOpened: 5,
    autoDismiss: true,
    newestOnTop: true,
    preventDuplicates: true,
    tapToDismiss: true,
    closeButton: true,
    enableHtml: true,
    progressBar: true,
    positionClass: 'toast-top-full-width'
};
