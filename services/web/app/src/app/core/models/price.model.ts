import {BaseModel} from "../base/base.model";

export class PriceModel extends BaseModel {
  week_day_start: number;
  week_day_end: number;
  hour_start: any;
  hour_end: any;
  price_per_hour: number;
}
