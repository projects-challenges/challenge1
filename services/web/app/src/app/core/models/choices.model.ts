import {BaseModel} from "../base/base.model";

export class Choices {
  value: number;
  display: string;
}
