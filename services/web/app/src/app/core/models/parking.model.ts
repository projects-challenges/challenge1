import {BaseModel} from "../base/base.model";

export class ParkingModel extends BaseModel {
  vehicle_number: string;
  model: string;
  color: string;
  entry_date: Date;
  departure_date: Date;
  revenue: number;
  price_exit?: number;
}
