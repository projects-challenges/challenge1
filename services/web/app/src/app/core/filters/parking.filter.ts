import {BaseFilter} from "../base/base.filter";

export class ParkingFilter extends BaseFilter {
  vehicle_number_model_color?: string;
  entry_date_after?: string;
  entry_date_before?: string;
  departure_date_after?: string;
  departure_date_before?: string;
}
