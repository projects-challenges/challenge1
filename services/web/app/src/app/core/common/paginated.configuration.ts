import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import {MatPaginatorIntl} from "@angular/material/paginator";

@Injectable()
export class PaginatedConfiguration extends MatPaginatorIntl {

  constructor(private translate: TranslateService) {
    super();

    this.translate.onLangChange.subscribe((e: Event) => {
      this.translateLabels();
    });

    this.translateLabels();
  }

  translateLabels() {
    this.itemsPerPageLabel = this.translate.instant('COMMON.PAGINATION.ITEMS_PER_PAGE');
    this.firstPageLabel = this.translate.instant('COMMON.PAGINATION.FIRST_PAGE');
    this.lastPageLabel = this.translate.instant('COMMON.PAGINATION.LAST_PAGE');
    this.nextPageLabel = this.translate.instant('COMMON.PAGINATION.NEXT_PAGE');
    this.previousPageLabel = this.translate.instant('COMMON.PAGINATION.PREVIOUS_PAGE');
    this.changes.next();
  }

  getRangeLabel = function(page, pageSize, length) {
    const _of = this.translate ? this.translate.instant('COMMON.PAGINATION.OF') : 'of';
    if (length === 0 || pageSize === 0) {
      return '0 ' + _of + ' ' + length;
    }
    length = Math.max(length, 0);
    const startIndex = page * pageSize;
    // If the start index exceeds the list length, do not try and fix the end index to the end.
    const endIndex = startIndex < length ?
      Math.min(startIndex + pageSize, length) :
      startIndex + pageSize;
    return startIndex + 1 + ' - ' + endIndex + ' ' + _of + ' ' + length;
  };

}
