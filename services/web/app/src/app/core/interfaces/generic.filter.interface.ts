export interface IGenericFilter<Z> {
  new(): Z;
}
