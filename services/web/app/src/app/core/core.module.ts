import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavComponent } from './components/nav/nav.component';
import {MaterialModule} from "../shared/material.module";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {HttpClientModule} from "@angular/common/http";
import {RouterModule} from "@angular/router";
import {TranslateModule} from "@ngx-translate/core";
import {MatPaginatorIntl} from "@angular/material/paginator";
import {PaginatedConfiguration} from "./common/paginated.configuration";
import {DialogComponent} from "./components/dialog/dialog.component";


@NgModule({
  declarations: [NavComponent, DialogComponent],
  imports: [
    BrowserAnimationsModule,
    MaterialModule,
    CommonModule,
    HttpClientModule,
    RouterModule,
    TranslateModule,
  ],
  exports: [
    TranslateModule,
    MaterialModule,
    DialogComponent,
  ],
  providers: [
    { provide: MatPaginatorIntl, useClass: PaginatedConfiguration }
  ],
  entryComponents: [DialogComponent],
})
export class CoreModule { }
