import {Component} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss']
})
export class NavComponent {

  public language = 'pt';

  constructor(
    public translate: TranslateService
  ) {
    this.translate.addLangs(['en', 'pt']);
    this.translate.setDefaultLang(this.language);
  }

  public switchLang($event) {
    let lang = $event;
    this.language = lang;
    this.translate.setDefaultLang(lang);
    this.translate.use(lang);
  }

}
