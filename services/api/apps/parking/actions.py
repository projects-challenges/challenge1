from django.db.models import Sum
from rest_framework import status
from rest_framework.response import Response
from apps.parking.filters import ParkingFilterSet
from apps.parking.models import Parking


def revenue(request):
    data = request.GET.copy()
    data["page"] = None
    data["limit"] = None
    data["offset"] = None

    query_filter = ParkingFilterSet(data, queryset=Parking.objects.all())
    query_aggregate = query_filter.qs.aggregate(total_revenue=Sum('revenue'))
    return Response(data={"revenue_total": query_aggregate['total_revenue']}, status=status.HTTP_200_OK)
