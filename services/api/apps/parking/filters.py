from django_filters import filterset, DateFromToRangeFilter
from django.db.models import Q

from apps.parking.models import Parking


class ParkingFilterSet(filterset.FilterSet):
    entry_date = DateFromToRangeFilter()
    departure_date = DateFromToRangeFilter()
    vehicle_number_model_color = filterset.CharFilter(method='filter_vehicle_number_model_color')

    @staticmethod
    def filter_vehicle_number_model_color(queryset, name, value):
        return queryset.filter(
            Q(vehicle_number__unaccent__icontains=value) |
            Q(model__unaccent__icontains=value) |
            Q(color__unaccent__icontains=value))

    class Meta:
        model = Parking
        fields = ['vehicle_number_model_color', 'entry_date', "departure_date"]
