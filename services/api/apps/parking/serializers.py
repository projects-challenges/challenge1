import math
from datetime import datetime
from rest_framework import serializers

from apps.parking.models import Parking
from apps.price.models import Price


class ParkingSerializer(serializers.ModelSerializer):
    price_exit = serializers.SerializerMethodField()

    @staticmethod
    def get_price_exit(self):
        if self.revenue:
            return None

        weekday = self.entry_date.isoweekday()
        time = self.entry_date.time()
        hours_in_parking = datetime.today() - self.entry_date
        hours_in_parking = math.ceil(hours_in_parking.total_seconds() / 3600)
        price = Price.objects.filter(week_day_end__gte=weekday, week_day_start__lte=weekday).filter(
            hour_end__gte=time, hour_start__lte=time)
        if price.count() == 0:
            price = Price.objects.filter(week_day_end__gte=weekday, week_day_start__lte=weekday).order_by(
                '-price_per_hour')
            if price.count() == 0:
                return None
        price = price.first()
        price_parking = hours_in_parking * price.price_per_hour

        return price_parking

    class Meta:
        model = Parking
        fields = '__all__'
