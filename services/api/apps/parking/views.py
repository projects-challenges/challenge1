from rest_framework import viewsets
from rest_framework.decorators import action

from apps.parking.actions import revenue
from apps.parking.filters import ParkingFilterSet
from apps.parking.models import Parking
from apps.parking.serializers import ParkingSerializer


class ParkingViewSet(viewsets.ModelViewSet):
    queryset = Parking.objects.order_by('-id')
    serializer_class = ParkingSerializer
    filter_class = ParkingFilterSet

    def list(self, request, *args, **kwargs):
        return super(ParkingViewSet, self).list(request, *args, **kwargs)

    @action(methods=['GET'], detail=False)
    def revenue_total(self, request, *args, **kwargs):
        return revenue(request)
