from datetime import datetime
from django.db import models
from apps.core import exceptions
from apps.price.models import Price


class Parking(models.Model):
    vehicle_number = models.CharField(max_length=50, null=False)
    model = models.CharField(max_length=50, null=False)
    color = models.CharField(max_length=50, null=False)
    entry_date = models.DateTimeField(null=True)
    departure_date = models.DateTimeField(null=True)
    revenue = models.DecimalField(max_digits=8, decimal_places=2, null=True)

    class Meta:
        db_table = 'apps_parking'

    def save(self, *args, **kwargs):
        try:
            self.vehicle_number = self.vehicle_number.upper()
            self.model = self.model.upper()
            self.color = self.color.upper()
            if self.id:
                parking = Parking.objects.filter(vehicle_number=self.vehicle_number, departure_date__isnull=True)
                if parking.count() > 0:
                    if self.id != parking.first().id:
                        raise exceptions.ParkingVehicleRegistered()
                    if self.revenue and self.departure_date is None:
                        self.departure_date = datetime.today()
            else:
                self.entry_date = datetime.today()
                price = Price.objects.filter(week_day_end__gte=self.entry_date.isoweekday(),
                                             week_day_start__lte=self.entry_date.isoweekday()).filter(
                    hour_end__gte=self.entry_date.time(), hour_start__lte=self.entry_date.time())
                if price.count() == 0:
                    raise exceptions.PriceWeekDayHourNoContent()

                parking = Parking.objects.filter(vehicle_number=self.vehicle_number, departure_date__isnull=True)
                if parking.count() > 0:
                    raise exceptions.ParkingVehicleRegistered()

            super(Parking, self).save(*args, **kwargs)
        except (RuntimeError, TypeError, NameError):
            pass
