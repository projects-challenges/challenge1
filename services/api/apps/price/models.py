from django.db import models
from apps.core import choices


class Price(models.Model):
    week_day_start = models.IntegerField(choices=choices.DAYS_OF_WEEK, null=False)
    week_day_end = models.IntegerField(choices=choices.DAYS_OF_WEEK, null=False)
    hour_start = models.TimeField(null=False)
    hour_end = models.TimeField(null=False)
    price_per_hour = models.DecimalField(max_digits=8, decimal_places=2, null=True)

    class Meta:
        db_table = 'apps_price'
        unique_together = ('week_day_start', 'week_day_end', 'hour_start', 'hour_end')
