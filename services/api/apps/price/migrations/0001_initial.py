# Generated by Django 3.1.1 on 2020-09-29 08:48

from django.db import migrations, models
from django.contrib.postgres.operations import UnaccentExtension
from apps.core.choices import DAYS_OF_WEEK

PRICE_DATA = [
    {
        'week_day_start': DAYS_OF_WEEK[0][0],
        'week_day_end': DAYS_OF_WEEK[4][0],
        'hour_start': '08:00',
        'hour_end': '12:00',
        'price_per_hour': 2.00
    },
    {
        'week_day_start': DAYS_OF_WEEK[0][0],
        'week_day_end': DAYS_OF_WEEK[4][0],
        'hour_start': '12:01',
        'hour_end': '18:00',
        'price_per_hour': 3.00
    },
    {
        'week_day_start': DAYS_OF_WEEK[5][0],
        'week_day_end': DAYS_OF_WEEK[6][0],
        'hour_start': '08:00',
        'hour_end': '18:00',
        'price_per_hour': 2.50
    }
]


def import_data(apps, schema_editor):
    price = apps.get_model('price', 'Price')
    for row in PRICE_DATA:
        new_price = price(week_day_start=row['week_day_start'], week_day_end=row['week_day_end'],
                          hour_start=row['hour_start'], hour_end=row['hour_end'],
                          price_per_hour=row['price_per_hour'], )
        new_price.save()


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Price',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('week_day_start', models.IntegerField(choices=[(1, 'MONDAY'), (2, 'TUESDAY'), (3, 'WEDNESDAY'), (4, 'THURSDAY'), (5, 'FRIDAY'), (6, 'SATURDAY'), (7, 'SUNDAY')])),
                ('week_day_end', models.IntegerField(choices=[(1, 'MONDAY'), (2, 'TUESDAY'), (3, 'WEDNESDAY'), (4, 'THURSDAY'), (5, 'FRIDAY'), (6, 'SATURDAY'), (7, 'SUNDAY')])),
                ('hour_start', models.TimeField()),
                ('hour_end', models.TimeField()),
                ('price_per_hour', models.DecimalField(decimal_places=2, max_digits=8, null=True)),
            ],
            options={
                'db_table': 'apps_price',
                'unique_together': {('week_day_start', 'week_day_end', 'hour_start', 'hour_end')},
            },
        ),
        migrations.RunPython(import_data),
        UnaccentExtension()
    ]
