# Create your views here.
from rest_framework import viewsets

from apps.price.models import Price
from apps.price.serializers import PriceSerializer


class PriceViewSet(viewsets.ModelViewSet):
    queryset = Price.objects.order_by('-id')
    serializer_class = PriceSerializer

