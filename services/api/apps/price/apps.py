from django.apps import AppConfig


class PriceConfig(AppConfig):
    name = 'apps.price'
