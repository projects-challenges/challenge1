from django.http import JsonResponse
from django.db import IntegrityError
from rest_framework.exceptions import APIException
from rest_framework import status, views
from rest_framework.response import Response

from apps.core import messages


def server_error(request, *args, **kwargs):
    """
    Generic 500 error handler.
    """
    data = {
        'detail': messages.error_http500
    }
    return JsonResponse(data, status=status.HTTP_500_INTERNAL_SERVER_ERROR)


def exception_handler(exc, context):
    response = views.exception_handler(exc, context)
    if isinstance(exc, IntegrityError):
        from psycopg2 import errorcodes as pg_errorcodes
        if exc.__cause__.pgcode == pg_errorcodes.FOREIGN_KEY_VIOLATION:
            return Response({'title': 'COMUNS.TOAST.TITLE.ERROR', 'detail': messages.foreign_key_violation,
                             'message': exc.__cause__.pgerror},
                            status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        elif exc.__cause__.pgcode == pg_errorcodes.UNIQUE_VIOLATION:
            return Response({'title': 'COMUNS.TOAST.TITLE.ERROR', 'detail': messages.unique_key_violation,
                             'message': exc.__cause__.pgerror},
                            status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        else:
            return Response({'detail': exc.__cause__.pgerror},
                            status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    if isinstance(exc, BaseAPIException):
        response.data['args'] = exc.args_list
    return response


class BaseAPIException(APIException):
    def __init__(self, message=None):
        self.args_list = []


class AlreadyException(BaseAPIException):
    def __init__(self, message=None):
        super().__init__(message)
        self.status_code = status.HTTP_409_CONFLICT
        if message == None:
            self.detail = messages.already_exist
        else:
            self.detail = message


class AlreadyUsedException(BaseAPIException):
    def __init__(self, message=None):
        super().__init__(message)
        self.status_code = status.HTTP_409_CONFLICT
        self.detail = messages.already_used


class AlreadyEvaluationException(BaseAPIException):
    def __init__(self, message=None):
        super().__init__(message)
        self.status_code = status.HTTP_409_CONFLICT
        self.detail = messages.evaluation_associated


class NoRecordsProcessedException(BaseAPIException):
    def __init__(self, message=None):
        super().__init__(message)
        self.status_code = status.HTTP_400_BAD_REQUEST
        self.detail = messages.no_records_processed


class AlreadyDateExistsException(BaseAPIException):
    def __init__(self, message=None):
        super().__init__(message)
        self.status_code = status.HTTP_409_CONFLICT
        self.detail = messages.dates_exists


class FieldEmptyException(BaseAPIException):
    def __init__(self, message=None):
        super().__init__(message)
        self.status_code = status.HTTP_412_PRECONDITION_FAILED
        self.detail = messages.field_not_empty


class DeleteException(BaseAPIException):
    def __init__(self, message=None):
        super().__init__(message)
        self.status_code = status.HTTP_403_FORBIDDEN
        self.detail = message


class NoContentException(BaseAPIException):
    def __init__(self, message=None):
        super().__init__(message)
        self.status_code = status.HTTP_204_NO_CONTENT
        self.detail = 'No content.'


class PriceWeekDayHourNoContent(BaseAPIException):
    def __init__(self, message=None):
        super().__init__(messages.price_no_content_weekday_hour)
        self.status_code = status.HTTP_412_PRECONDITION_FAILED
        self.detail = messages.price_no_content_weekday_hour


class ParkingVehicleRegistered(BaseAPIException):
    def __init__(self, message=None):
        super().__init__(messages.parking_vehicle_registered)
        self.status_code = status.HTTP_412_PRECONDITION_FAILED
        self.detail = messages.parking_vehicle_registered
